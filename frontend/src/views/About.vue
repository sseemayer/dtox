<template lang='pug'>
  v-container#about
    h1.display-4 About D/Tox
    p D/Tox is based on a collection of {{nMolecules}} molecules that are either approved drugs or molecules that trigger the <a href='https://en.wikipedia.org/wiki/Ames_test' target='_blank'>Ames test</a> for DNA mutagenicity. You are presented with a random selection of molecules from this data set and have to guess whether the molecule is an approved drug or Ames mutagen.

    h2.display-3 Materials and Methods

    h3.display-2 Data Sources

    h4.display-1 CHEMBL Drugs
    p Approved drugs were downloaded from the March 2018 version of the <a href='https://www.ebi.ac.uk/chembl/drugstore'>CHEMBL Drugs</a> database&nbsp;<cite>[1]</cite> using the SDF format (11628 molecules in total). All drugs with a <code>DRUG_TYPE</code> property of <code>antibody</code> or <code>polypeptide</code> were ignored since they are not small molecules. All drugs with a non-empty <code>WITHDRAWN_REASON</code> were ignored since they might not be a safe drug according to the state of the art.

    p In order to give a bit more flavor to the player what the different drugs are used for, the <code>INDICATION_CLASS</code>, <code>USAN_STEM_DEFINITION</code> and <code>ATC_CODE_DESCRIPTION</code> properties were also extracted to give the player an indication about the drug's indication.

    h4.display-1 Ames Toxicity
    p Ames toxins were downloaded from the version 1 of the <a href='http://doc.ml.tu-berlin.de/toxbenchmark/'>Benchmark dataset for in silico prediction of Ames mutagenicity.</a>&nbsp;<cite>[2]</cite> using both the SDF and CSV format (7090 molecules in total). All drugs with an activity of <code>0</code> were removed since they did not show Ames toxicity.

    p As there is only a CAS number available inside of the Ames toxicity data set, the CAS numbers were queried against the PubChem Substance database&nbsp;<cite>[3]</cite> to extract additional synonyms that could be displayed as a 'drug name'. If you have a good way for picking a human-friendly synonym from the list of available synonyms, please let me know!

    h3.display-2 Data Processing
    p All data processing was done in <a href='https://jupyter.org/' target='_blank'>Jupyter Notebook</a> using <a href='https://pandas.pydata.org/' target='_blank'>Pandas</a> for general data processing, <a href='https://matplotlib.org/' target='_blank'>Matplotlib</a> for plotting and <a href='https://www.rdkit.org/' target='_blank'>RDKit</a>&nbsp;<cite>[4]</cite> for all chemoinformatics needs.

    h4.display-1 Filtering for Suitable Molecules
    p Not all molecules in the dataset make good small molecules for the purpose of this game; as a simple base filter, all molecules that had a molecular weight below 20 g/mol or a molecular weight above 800 g/mol were removed. This only leaves the set of "well-behaved" molecules between the two red lines in the below histogram:

    img(src='molecular_weight_distribution.svg' alt='Molecular Weight Distribution' width='90%' style='margin: 0 auto; display: block')

    p Additionally, a few molecules caused errors in the later drawing step and were removed by taking them out at this point and re-running the pipeline below.

    p After filtering, 8496 drugs and 3753 ames toxins were left in the dataset.

    h4.display-1 Clustering to Remove Redundancy
    p Additionally, we want to make sure that some structural elements are not too overrepresented in the data set but that we rather present a diverse set of different molecules to the player. A way to ensure this is to cluster the molecules into groups of similar molecules and then only keep one representative from each cluster.

    p The <a href='https://www.rdkit.org/docs/Cookbook.html#clustering-molecules' target='_blank'>RDKit cookbook on clustering</a> was followed for this step, throwing all molecules from both groups that survived filtering and cluster together: Molecules were represented using Morgan fingerprints (<code>rdkit.Chem.AllChem.GetMorganFingerprintsAsBitVect(m, 2, nBits=1024)</code>) when comparing them using the Tanimoto similarity score (<code>rdkit.DataStructs.BulkTanimotoSimilarity</code>). The molecules were than clustered using the <code>rdkit.ML.Cluster.Butina</code> clustering algorithm and cluster centroids picked as cluster representatives.

    p After clustering, 7730 drugs and 3243 ames toxins were left in the dataset.

    h4.display-1 Subsampling to Balance Classes
    p Since we have more drugs than ames toxins in the dataset, the player could get an inflated score by simply guessing 'Cure' on every molecule. In order to make sure that this strategy is not viable, random subsampling is performed to ensure that the same number of drugs and toxins remain in the dataset &mdash; 3243 drugs are picked randomly out of the 7730 total drugs to match the 3243 ames toxins. This way, the player has a 50-50 chance when picking randomly.

    p After subsampling, 3243 drugs and 3243 ames toxins were left in the dataset.

    h4.display-1 Drawing Molecules
    p After using <code>rdkit.Chem.AllChem.Compute2DCoords(m)</code> to generate 2D coordinates of the atoms in all molecules, <code>rdkit.Chem.Draw.MolToFile</code> can be used to draw SVG images of all molecules in the dataset.
    p Unfortunately, RDKit will create a white background rectangle that prevents further cropping; to get rid of these, a custom python script was used to delete all elements with white fill from the molecule's SVG XML files. Now, Inkscape can be used to <a href='https://shkspr.mobi/blog/2013/03/inkscape-cropping-svg-files-on-the-command-line/' target='_blank'>crop the SVG images</a> using the command line (with the slight disadvantage of opening and closing an Inkscape window for every image):
    pre.mb-4: code.
      for fn in *.svg; do
        inkscape --verb=FitCanvasToDrawing --verb=FileSave --verb=FileQuit $fn
      done
    h3.display-2 Web Development

    h4.display-1 Molecules for the Web
    p After rendering out the molecules via RDKit and cropping the images, <a href='https://github.com/svg/svgo' target='_blank'>SVGo</a> was used to reduce image file sizes (an average 50% reduction of file sizes was observed).
    p The data for all molecules was exported as JSON files so that rdkit is not necessary as a run-time dependency for the API and no calculations on the molecules needs to be done at run time - this way, the backend becomes very simple and fast.

    h4.display-1 Background Video

    p The background video is <a href='https://www.videezy.com/health-and-beauty/7767-medicine-packages' target='_blank'>Medicine Packages</a> which was downloaded for free from <a href='http://www.videezy.com' target='_blank'>Videezy</a>.

    p <a href='https://kdenlive.org/en/' target='_blank'>Kdenlive</a> was used to cut the video and make it bounce between playing in forward and reverse, then <a href='https://handbrake.fr/' target='_blank'>Handbrake</a> was used to encode the video into low-filesize H.264. The blur effect on the background hides most of the compression artifacts :)

    h4.display-1 Technology Stack
    p The D/Tox backend is written in Python 3 using the <a href='http://flask.pocoo.org/' target='_blank'>Flask</a> framework. Since no user data is stored, the backend just amounts to statically serving images and reading JSON files for each of the molecules to find out if the user guessed correctly.

    p The D/Tox frontend is a <a href='https://vuejs.org/' target='_blank'>Vue</a> 2 application using <a href='https://vuex.vuejs.org/' target='_blank'>vuex</a> and <a href='https://router.vuejs.org/' target='_blank'>vue-router</a>. The user interface is based on <a href='https://vuetifyjs.com/' target='_blank'>Vuetify</a> and HTTP requests to the backend API are dispatched with <a href='https://github.com/axios/axios' target='_blank'>axios</a>. HTML templates are written in <a href='https://pugjs.org/' target='_blank'>pug</a> and custom styles are written in <a href='http://stylus-lang.com/' target='_blank'>stylus</a>, with both being statically compiled into the Vue components using the <a href='https://webpack.js.org/' target='_blank'>Webpack</a> workflow of the <a href='https://cli.vuejs.org/' target='_blank'>vue-cli-service</a>.

    h4.display-1 Source Code?

    p Have a look at&nbsp;
      a(href='https://gitlab.com/sseemayer/dtox' target='_blank') my GitLab
      | .

    h3.display-2 Citations

    dl.citations

      dt: cite [1]
      dd: cite
        .authors Gaulton A, Hersey A, Nowotka M, Bento AP, Chambers J, Mendez D, Mutowo P, Atkinson F, Bellis LJ, Cibrián-Uhalte E, Davies M, Dedman N, Karlsson A, Magariños MP, Overington JP, Papadatos G, Smit I, Leach AR.
        .year (2017)
        .title The ChEMBL database in 2017
        .journal Nucleic Acids Research
        .volume-issue 45(D1)
        .page D945-D954

        .x-refs
          .doi: a(href='https://dx.doi.org/10.1093/nar/gkw1074' target='_blank') 10.1093/nar/gkw1074
          .pmc: a(href='https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5210557/' target='_blank') PMC5210557

      dt: cite [2]
      dd: cite
        .authors Hansen K, Mika S, Schroeter T, Sutter A, ter Laak A, Steger-Hartmann T, Heinrich N, M&uuml;ller KR.
        .year (2009)
        .title Benchmark Data Set for in Silico Prediction of Ames Mutagenicity
        .journal Journal of Chemical Information and Modeling

        .x-refs
          .doi: a(href='http://dx.doi.org/10.1021/ci900161g' target='_blank') 10.1021/ci900161g

      dt: cite [3]
      dd: cite
        .authors Kim S, Thiessen PA, Bolton EE, Chen J, Fu G, Gindulyte A, Han L, He J, He S, Shoemaker BA, Wang J, Yu B, Zhang J, Bryant SH.
        .year (2016)
        .title PubChem Substance and Compound databases
        .journal Nucleic Acids Research
        .volume-issue 44(D1)
        .page D1202-13.

        .x-refs
          .doi: a(href='https://dx.doi.org/10.1093/nar/gkv951' target='_blank') 10.1093/nar/gkv951

      dt: cite [4]
      dd: cite
        .authors Landrum G
        .title RDKit: Open-source chemoinformatics
        .x-refs
          .url: a(href='http://www.rdkit.org/' target='_blank') www.rdkit.org

    h2.display-3 Playing
    h3.display-2 Privacy
    p D/Tox will send your guesses to a server and respond with a reply but nothing will be connected back to your identity and no personally-identifiable records will be kept.

    h3.display-2 Terms of Use
    p No guarantees are made for the correctness of any of the information contained in the game and no responsibility for outbound links is taken.

    h3.display-1 Credits
    p D/Tox was created by <a href='https://stefan.seemayer.de' target='_blank'>Stefan Seemayer</a> in December 2018 based on the idea by <a href='https://mochimochiyarns.com/' target='_blank'>Adriana Guadron</a>. Many thanks to Miriam Mathea for pointing us into the right direction on where to get data!

</template>

<script>
export default {
  mounted () {
    this.$store.dispatch('getStats')
  },
  computed: {
    nMolecules () {
      return this.$store.state.statistics.n_molecules
    }
  }
}
</script>

<style lang='stylus' scoped>
  #about
    max-width 900px

  .citations
    margin-top 24px

  dt
    float left
    width 40px
    clear left
  dd
    float left
    max-width 810px

  cite
    font-style normal !important
    font-weight bold

  dd cite
    font-weight normal

  cite

    .authors, .year, .journal, .volume-issue, .page
      display inline

    .title
      font-size 130% !important
      font-style normal !important

    .journal:after, .volume-issue:after
      content ' '

    .doi, .pmc, .url
      &:before
        display inline-block
        width 40px
        font-style normal !important

    .doi:before
      content 'DOI: '

    .pmc:before
      content 'PMC: '

    .url:before
      content 'URL: '

    .x-refs
      margin 10px 0 20px 0

  h2
    clear both
</style>
