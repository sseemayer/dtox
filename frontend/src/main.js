import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.config.productionTip = false

Vue.filter('shortSynonym', (syn) => syn.split(';')[0].replace(/\s*\(.*?\)\s*$/g, '') || 'Unnamed compound')
Vue.filter('prettyActivity', (act) => act ? 'Kill' : 'Cure')
Vue.filter('iconActivity', (act) => act ? 'smoking_rooms' : 'local_pharmacy')
Vue.filter('detailsURL', (name) => name.substr(0, 6) === 'CHEMBL'
  ? `https://www.ebi.ac.uk/chembldb/index.php/compound/inspect/${name}`
  : `https://toxgate.nlm.nih.gov/cgi-bin/sis/search2/r?dbs+toxline:@rn+${name}`)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
