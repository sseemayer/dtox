import Vue from 'vue'
import Vuex from 'vuex'

import axios from 'axios'

import router from './router'

import { getPrompt } from './prompts'

Vue.use(Vuex)

const API_URL = '/api'

export default new Vuex.Store({
  state: {
    molecules: [],
    answers: [],
    question: null,
    currentStep: null,
    loading: false,
    statistics: {}
  },
  mutations: {
    setLoading (state, v) {
      state.loading = v
    },
    setupStatistics (state, { statistics }) {
      state.statistics = statistics
    },
    setupGame (state, { molecules }) {
      state.question = getPrompt(state)
      state.molecules = molecules
      state.answers = []
      state.currentStep = 0
    },
    guessAndAnswer (state, { answer }) {
      state.answers.push(answer)
    },
    continue (state) {
      state.currentStep += 1
      state.question = getPrompt(state)
    }
  },
  getters: {
    currentMolecule: (state) => state.molecules[state.currentStep],
    currentAnswer: (state) => state.answers[state.currentStep]
  },
  actions: {
    getStats (ctx) {
      axios.get(`${API_URL}/statistics`)
        .then((r) => {
          ctx.commit('setupStatistics', { statistics: r.data })
        })
    },
    newGame (ctx, { nMolecules }) {
      ctx.commit('setLoading', true)
      axios.get(`${API_URL}/game/${nMolecules}`)
        .then((r) => {
          ctx.commit('setupGame', { molecules: r.data.molecules || 10 })
          ctx.commit('setLoading', false)

          router.push('play')
        })
    },
    makeGuess (ctx, { moleculeId, guess }) {
      ctx.commit('setLoading', true)
      axios.post(`${API_URL}/guess/${moleculeId}`, { guess: guess })
        .then((r) => {
          ctx.commit('guessAndAnswer', {
            answer: r.data
          })
          ctx.commit('setLoading', false)
        })
    }
  }
})
