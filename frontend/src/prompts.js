
const PROMPTS = [
  {
    message: 'What do you think about this one?',
    condition: (state) => true
  },
  {
    message: 'How about this one?',
    condition: (state) => state.currentStep > 0
  },
  {
    message: 'Do you think that this is good for you?',
    condition: (state) => true
  },
  {
    message: 'Drug or Mutagen?',
    condition: (state) => true
  },
  {
    message: 'Almost done!',
    condition: (state) => state.currentStep / state.molecules.length >= 0.8
  }
]

export function getPrompt (state) {
  const candidates = PROMPTS.filter((p) => p.condition(state))
  const selection = candidates[Math.floor(Math.random() * candidates.length)]
  return selection.message
}
