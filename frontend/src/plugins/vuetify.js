import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'
import Touch from 'vuetify/es5/directives/touch'

Vue.use(Vuetify, {
  theme: {
    primary: '#00BCD4',
    secondary: '#80DEEA',
    accent: '#303F9F',
    error: '#f44336',
    warning: '#ffeb3b',
    info: '#2196f3',
    success: '#4caf50'
  },
  customProperties: true,
  iconfont: 'md',
  directives: { Touch }
})
