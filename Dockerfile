FROM node:alpine as vue

# setup /frontend and install dependencies
RUN mkdir /frontend
ADD frontend/package.json frontend/yarn.lock /frontend/
WORKDIR /frontend/
RUN yarn install

# add sourcecode and build vue
ADD frontend /frontend
RUN yarn build


FROM python:alpine

# setup /app and install dependencies
RUN mkdir /app
ADD backend/requirements.txt /app/
WORKDIR /app/

RUN pip install -r requirements.txt

# add frontend
COPY --from=vue /frontend/dist /app/static

# add sourcecode
ADD backend /app



# run app
CMD ["gunicorn", "-b", "0.0.0.0:8000", "-w", "4", "server:app", "--access-logfile", "-"]
