#!/usr/bin/env python

import glob
import random
import json
import os

from flask import Flask, jsonify, send_file, request

app = Flask(__name__, static_url_path='')

MOLECULES = [
    fn.replace('labels/', '').replace('.json', '')
    for fn in glob.glob('labels/*.json')
]

app.logger.info('Got {} molecules'.format(len(MOLECULES)))


class InvalidParameter(Exception):
    """Exception class indicating an error in a user-specified parameter"""
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code

        self.payload = payload

    def to_dict(self):
        """Dictify the exception"""
        ret = dict(self.payload or ())
        ret['message'] = self.message
        return ret


@app.errorhandler(InvalidParameter)
def handle_invalid_usage(error):
    """Error handler for invalid parameters"""
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

@app.route('/')
def index():
    return send_file('static/index.html')

@app.route('/api/statistics')
def statistics():
    return jsonify({
        'n_molecules': len(MOLECULES),
    })

@app.route('/api/game/<int:n_molecules>')
def generate_batch(n_molecules=10):
    """Start a new game by generating a batch of n molecules"""

    if not 5 <= n_molecules <= 100:
        raise InvalidParameter('Must request between 5 and 100 molecules!')

    if n_molecules > len(MOLECULES):
        raise InvalidParameter("Don't have enough molecules!")

    mols = random.sample(MOLECULES, n_molecules)

    app.logger.info('New game with {} molecules: {}'.format(n_molecules, mols))

    return jsonify({'molecules': mols})


@app.route('/api/image/<molecule_id>')
def show_image(molecule_id):
    """Depict a molecule"""

    if molecule_id not in MOLECULES:
        raise InvalidParameter('Molecule ID "{}" is invalid'.format(molecule_id))

    return send_file('images/{}.svg'.format(molecule_id))


@app.route('/api/guess/<molecule_id>', methods=['POST'])
def guess_molecule(molecule_id):
    """Let the player make a guess"""

    if molecule_id not in MOLECULES:
        raise InvalidParameter('Molecule ID "{}" is invalid'.format(molecule_id))

    guess = request.json.get('guess') or request.form.get('guess')
    if guess not in ('active', 'inactive'):
        raise InvalidParameter('Valid guesses are "active" and "inactive"')

    guess = guess == 'active'

    with open('labels/{}.json'.format(molecule_id), 'r') as f_truth:
        truth = json.load(f_truth)

    app.logger.info('Guessed that {} is {} - truth: {}'.format(molecule_id, guess, truth['active']))

    return jsonify({
        'moleculeId': molecule_id,
        'guessedActivity': guess,
        'trueActivity': truth['active'],
        'name': truth['name'],
        'synonyms': truth['synonyms'],
        'indication': truth['indication'],
        'usan': truth['usan'],
        'atc': truth['atc']
    })
